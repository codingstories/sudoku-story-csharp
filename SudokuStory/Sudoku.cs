﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SudokuStory.Exceptions;

namespace SudokuStory
{
    public class Sudoku
    {
        private static readonly List<int> ALL_DIGITS = new List<int>() { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
        private static readonly int FIRST_SECTOR_START = 0;            
        private static readonly int MIDDLE_SECTOR_START = 3;            
        private static readonly int LAST_SECTOR_START = 6;
        private static readonly int MAX_DIGIT = 9;

        private string input;

        private StringBuilder result = new StringBuilder();
        private string solution = "";
        private bool noTableAction = true;
        private bool isSolved;

        private int[,] table = new int[MAX_DIGIT, MAX_DIGIT];

        public void FindSimpleSolution()
        {
            InitTableCellsFromInput();
            FindSolutionOfTable();
        }

        private void FindSolutionOfTable()
        {
            try
            {
                TryFindSimpleSolution();
                AddSolution();
                result.Append("We did it ! Congratulations !\n" + "Simple!");
            }

            catch (NoCellVariantsException e)
            {
                result.Append("ERROR: input is not a sudoku\n");
            }

            catch (ComplexSudokuException e)
            {
                result.Append("Too complex sudoku");
            }
        }

        private void InitTableCellsFromInput()
        {
            String[] rows = input.Split('\n');
            ForEachCell((i, j) => {
                char c = rows[i].ToCharArray()[j];
                if (c == ' ')
                {
                    table[i, j] = 0;
                }
                else if (Char.GetNumericValue(c) <= MAX_DIGIT && Char.GetNumericValue(c) >= 0)
                {
                    table[i, j] = (int)Char.GetNumericValue(c);
                }
                else
                    throw new ArgumentException("Wrong input format");
            });
        }

        private int TryFindSolvedCell(int str, int col)
        {
            List<int> variants = GetCellVariants(str, col);
            if (IsMany(variants))
                return 0;
            if (variants.Count == 0)
                throw new NoCellVariantsException();
            return GetSingle(variants);
        }

        private int GetSingle(List<int> variants)
        {
            return variants.First();
        }            
                    
        private bool IsMany(List<int> variants)
        {
            return variants.Count > 1;
        }

        private List<int> GetCellVariants(int str, int col)
        {
            List<int> variants = new List<int>(ALL_DIGITS);
            List<int> variantsToExclude = new List<int>();
            variantsToExclude.AddRange(GetSolvedByRow(col));
            variantsToExclude.AddRange(GetSolvedByColumn(str));
            variantsToExclude.AddRange(GetSolvedBySector(str, col));
            variants.RemoveAll(i => variantsToExclude.Contains(i));
            return variants;
        }

        private List<int> GetSolvedBySector(int str, int col)
        {
            List<int> variants = new List<int>();

            ForEachCellInSector(str, col, (i, j) => {
                if (IsSolvedCell(i, j))
                    variants.Add(table[i, j]);
            });

            return variants;
        }

        private void ForEachCellInSector(int str, int col, Action<int, int> action)
        {
            int rowStart = GetStartSectorIndex(str);
            int columnStart = GetStartSectorIndex(col);

            for (int i = rowStart; i <= rowStart + 2; i++)
                for (int j = columnStart; j <= columnStart + 2; j++)
                    action.Invoke(i, j);
        }

        private int GetStartSectorIndex(int n) {    
            if (n <= MIDDLE_SECTOR_START - 1)
            {
                return FIRST_SECTOR_START;
            }

            if (n <= LAST_SECTOR_START - 1)
            {
                return MIDDLE_SECTOR_START;
            }

            return LAST_SECTOR_START;
        }

        private List<int> GetSolvedByColumn(int str)
        {
            List<int> variants = new List<int>();
            for (int j = 0; j < MAX_DIGIT; j++)
                if (IsSolvedCell(str, j))
                    variants.Add(table[str, j]);
            return variants;
        }

        private bool IsSolvedCell(int str, int col)
        {
            return table[str, col] != 0;
        }

        private List<int> GetSolvedByRow(int col)
        {
            List<int> variants = new List<int>();
            for (int i = 0; i < MAX_DIGIT; i++)
                if (IsSolvedCell(i, col))
                    variants.Add(table[i, col]);
            return variants;
        }

        private void AddSolution()
        {
            StringBuilder s = new StringBuilder();
            for (int i = 0; i < MAX_DIGIT; i++)
            {
                for (int j = 0; j < MAX_DIGIT; j++)
                {
                    s.Append(table[i, j]);
                }
                s.Append("\n");
            }
            solution = s.ToString();
        }

        private void TryFindSimpleSolution()
        {
            for (; ; )
            {
                if (IsSolved())
                {
                    return;
                }

                TrySolveSudoku();
                AssertActionPerformed();
            }
        }

        private void AssertActionPerformed()
        {
            if (noTableAction)
            {
                throw new ComplexSudokuException();
            }
        
        }

        private void TrySolveSudoku()
        {
            noTableAction = true;
            ForEachCell((i, j) => {
                if (IsNotSolvedCell(i, j))
                    TrySolveCell(i, j);
            });
        }

        private void ForEachCell(Action<int, int> action)
        {
            for (int i = 0; i < MAX_DIGIT; i++)
                for (int j = 0; j < MAX_DIGIT; j++)
                {
                    action.Invoke(i, j);
                }
        }

        private bool IsNotSolvedCell(int i, int j)
        {
            return !IsSolvedCell(i, j);
        }

        private void TrySolveCell(int i, int j)
        {
            var solvedCell = TryFindSolvedCell(i, j);
            if (solvedCell > 0)
            {
                table[i, j] = solvedCell;
                noTableAction = false;
            }
        }

        private bool IsSolved()
        {
            isSolved = true;
            ForEachCell((i, j) => {
                if (IsNotSolvedCell(i, j))
                    isSolved = false;
            });

            return isSolved;
        }

        public string GetResult()
        {
            return result.ToString();
        }

        public string GetSolution()
        {
            return solution;
        }

        public void SetInput(string input)
        {
            this.input = input;
        }
    }
}
