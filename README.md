# Garage Service Story

**To read**: [https://refactoringstories.com/file-view/https:%2F%2Fgit.epam.com%2FRefactoring-Stories%2Fsudoku-story-csharp]

## Story Outline
This story contains sudoku resolver which has
some problems like return codes, comments, DRY
and not very small function size

## Story Organization
**Story Branch**: master
>`git checkout master`

**Practical task tag for self-study**:task
>`git checkout task`

Tags: #clean_code