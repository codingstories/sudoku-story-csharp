﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SudokuStory.Tests
{
    [TestClass]
    public class SudokuTest
    {
        private Sudoku sudoku = new Sudoku();

        [TestMethod]
        public void ShouldNotSolveComplexSudoku()
        {
            sudoku.SetInput("  53     \n" +
                            "8      2 \n" +
                            " 7  1 5  \n" +
                            "4    53  \n" +
                            " 1  7   6\n" +
                            "  32   8 \n" +
                            " 6 5    9\n" +
                            "  4    3 \n" +
                            "     97  ");

            sudoku.FindSimpleSolution();

            Assert.AreEqual("Too complex sudoku", sudoku.GetResult());
            Assert.AreEqual("", sudoku.GetSolution());
        }

        [TestMethod]
        public void ShouldSolveSimpleSudoku()
        {
            sudoku.SetInput("2  5  8 3\n" +
                            "  6      \n" +
                            "51   2 49\n" +
                            "46    9 5\n" +
                            "   1 3   \n" +
                            " 21     7\n" +
                            " 3 4   62\n" +
                            "   3  5  \n" +
                            "6 7  8  4");

            sudoku.FindSimpleSolution();

            Assert.AreEqual("We did it ! Congratulations !\n" +
                         "Simple!", sudoku.GetResult());
            Assert.AreEqual("294561873\n" +
                         "376849251\n" +
                         "518732649\n" +
                         "463287915\n" +
                         "785193426\n" +
                         "921654387\n" +
                         "839415762\n" +
                         "142376598\n" +
                         "657928134\n", sudoku.GetSolution());
        }

        [TestMethod]
        public void ShouldNotSolveMoreComplexSudoku()
        {
            sudoku.SetInput("   5  8 3\n" +
                            "3768492 1\n" +
                            "       4 \n" +
                            " 6      5\n" +
                            "   1 3   \n" +
                            " 2       \n" +
                            "   4     \n" +
                            "      5  \n" +
                            "6    8  4");

            sudoku.FindSimpleSolution();
            Assert.AreEqual("Too complex sudoku", sudoku.GetResult());
        }

        [TestMethod]
        public void ShouldPrintErrorWhenSudokuIsWrong()
        {
            sudoku.SetInput("1  5  8 3\n" +
                            "  6 49   \n" +
                            "51   2 49\n" +
                            "46    915\n" +
                            "   1 3   \n" +
                            "921    87\n" +
                            "83 4   62\n" +
                            "   37 5  \n" +
                            "6 7  8  4");

            sudoku.FindSimpleSolution();
            Assert.AreEqual("ERROR: input is not a sudoku\n", sudoku.GetResult());
            Assert.AreEqual("", sudoku.GetSolution());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
            "Wrong input format")]
        public void ShouldTrowExceptionWhenInputContainsNotDigitsOrSpaces()
        {
            sudoku.SetInput("1  5  8 3\n" + "tk6 49   \n");
            sudoku.FindSimpleSolution();
        }

        [TestMethod]
        [ExpectedException(typeof(IndexOutOfRangeException))]
        public void ShouldTrowExceptionWhenNotEnoughInput()
        {
            sudoku.SetInput("1  5  8 3\n" + "  6 49   \n");
            sudoku.FindSimpleSolution();
        }
    }
}
